#include <iostream>
#include <stack>

using namespace std;
static const int MaxW = 200;
static const int nieskonczonosc = 2147483647;
void wypisz(bool tablicaKrawedzi[MaxW][MaxW], int deg[MaxW], int liczbaWierzcholkow)
{
	for (int i = 0; i < liczbaWierzcholkow; i++) {
		for (int j = 0; j < liczbaWierzcholkow; j++) {
			if (j == 0) cout << i % 10 << ": ";

			if (tablicaKrawedzi[i][j] == true)
				cout << "t ";
			else
				cout << "  ";

			if (j == liczbaWierzcholkow - 1)
				cout << "deg: " << deg[i] << endl;
		}
	}
	cout << endl;

}
int usunMinKrawedz(bool tablicaKrawedzi[MaxW][MaxW], int deg[MaxW], int liczbaWierzcholkow, const int skad)
{
	int minInd = nieskonczonosc;
	int minWart = 0;
	for (int i = liczbaWierzcholkow - 1; i >= 0; --i)
	{
		if (tablicaKrawedzi[skad][i] == true)
		{
			if (deg[i] == 1)
			{
				minInd = i;
				minWart = deg[i];

				tablicaKrawedzi[skad][i] = false;
				tablicaKrawedzi[i][skad] = false;

				--deg[i];
				--deg[skad];

				return minInd;
			}
			if (deg[i] < minInd)
			{
				minInd = i;
				minWart = deg[i];
			}
		}
	}
	tablicaKrawedzi[skad][minInd] = false;
	tablicaKrawedzi[minInd][skad] = false;

	--deg[minInd];
	--deg[skad];

	return minInd;
}

void fleury(bool tablicaKrawedzi[MaxW][MaxW], int deg[MaxW], int liczbaWierzcholkow)
{
	stack <int> stos;
	stack <int> ce;
	int v;

	stos.push(0);

	while (stos.size() != 0)
	{
		v = stos.top();
		if (deg[v] != 0)
		{
			int u = usunMinKrawedz(tablicaKrawedzi, deg, liczbaWierzcholkow, v);
			stos.push(u);
		}
		else
		{
			stos.pop();
			ce.push(v);
		}
	}

	while (ce.size() != 0)
	{
		cout << ce.top() << " ";
		ce.pop();
	}
	cout << "\n" << endl;
}

int main() {
	ios_base::sync_with_stdio();

	int ilosc_testow, liczbaWierzcholkow, liczbaKrawedzi, skad, dokad, pos;

	bool tablicaKrawedzi[MaxW][MaxW] = { 0 };
	int deg[MaxW] = { 0 };
	//Init na false

	cin >> ilosc_testow;

	for (int i = 0; i < ilosc_testow; ++i)
	{
		cin.ignore(256, '=');
		cin >> liczbaWierzcholkow;
		cin.ignore(3);
		cin >> liczbaKrawedzi;

		for (int j = 0; j < liczbaKrawedzi; ++j)
		{
			cin.ignore(256, '{');
			cin >> skad;
			cin.ignore(1);
			cin >> dokad;

			tablicaKrawedzi[skad][dokad] = true;
			tablicaKrawedzi[dokad][skad] = true;
			deg[skad] += 1;
			deg[dokad] += 1;
		}
		fleury(tablicaKrawedzi, deg, liczbaWierzcholkow);
	}

	return 0;
}